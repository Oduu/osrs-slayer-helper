-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: osrs_slayer
-- ------------------------------------------------------
-- Server version	5.7.28-0ubuntu0.18.04.4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `assignment`
--

DROP TABLE IF EXISTS `assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assignment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `npc_name` varchar(45) NOT NULL,
  `level_req` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assignment`
--

LOCK TABLES `assignment` WRITE;
/*!40000 ALTER TABLE `assignment` DISABLE KEYS */;
INSERT INTO `assignment` VALUES (1,'Aberrant Spectres',60),(2,'Abyssal Demons',85),(3,'Ankous',1),(4,'Banshees',15),(5,'Basilisks',40),(6,'Bats',1),(7,'Bears',1),(8,'Birds',1),(9,'Black Demons',1),(10,'Black Dragons',1),(11,'Bloodvelds',50),(12,'Blue Dragons',1),(13,'Brine Rats',47),(14,'Bronze Dragons',1),(15,'Catablepon',1),(16,'Cave Bugs',7),(17,'Cave Crawlers',10),(18,'Cave Horrors',58),(19,'Cave Slimes',17),(20,'Cockatrices',25),(21,'Cows',1),(22,'Crawling Hands',5),(23,'Crocodiles',1),(24,'Dagannoths',1),(25,'Dark Beasts',90),(26,'Dogs',1),(27,'Dust Devils',65),(28,'Dwarfs',1),(29,'Earth Warriors',1),(30,'Elves',1),(31,'Fever Spiders',42),(32,'Fire Giants',1),(33,'Flesh Crawlers',1),(34,'Fossil Islands Wyverns',66),(35,'Gargoyles',75),(36,'Ghosts',1),(37,'Ghouls',1),(38,'Goblins',1),(39,'Goraks',1),(40,'Greater Demons',1),(41,'Green Dragons',1),(42,'Harpie Bug Swarms',33),(43,'Hellhounds',1),(44,'Hill Giant',1),(45,'Hobgoblins',1),(46,'Hydras',95),(47,'Icefiends',1),(48,'Ice Giants',1),(49,'Ice Warriors',1),(50,'Infernal Mages',45),(51,'Iron Dragons',1),(52,'Jellies',52),(53,'Jungle Horrors',1),(54,'Lizardmen',1),(55,'Lizards',22),(56,'Kalphites',1),(57,'Killerwatts',37),(58,'Kurasks',70),(59,'Lesser Demons',1),(60,'Mithril Dragons',1),(61,'Minotaurs',1),(62,'Mogres',32),(63,'Molanisks',39),(64,'Monkeys',1),(65,'Moss Giants',1),(66,'Nechryael',80),(67,'Ogres',1),(68,'Otherworldly Beings',1),(69,'Pyrefiends',30),(70,'Red dragons',1),(71,'Rockslugs',20),(72,'Scabarites',1),(73,'Scorpions',1),(74,'Sea Snakes',1),(75,'Shades',1),(76,'Shadow Warriors',1),(77,'Skeletal Wyverns',72),(78,'Skeletons',1),(79,'Smoke Devils',93),(80,'Spiders',1),(81,'Steel Dragons',1),(82,'Suqahs',1),(83,'Terror Dogs',40),(84,'Trolls',1),(85,'Turoths',55),(86,'Vampyres',1),(87,'Wall Beasts',35),(88,'Waterfiends',1),(89,'Werewolves',1),(90,'Wolves',1),(91,'Zombies',1),(92,'Zygomites',57);
/*!40000 ALTER TABLE `assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES (1,'odu','test','reddinghf@gmail.com','2020-01-18 18:56:32');
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-19 22:34:02
