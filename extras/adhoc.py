# mainly functions revolved around osrsbox information to insert into DB
class adhoc_functions:
	def item_file_test():
		noDup = []
		path = "/home/odu/Desktop/Programming/osrs-slayer-helper/backend/static/items-complete.json"
		with open(path, "rb") as json_file:
			data = json.load(json_file)
			for key, value in data.items():
				if value['equipable_by_player'] == False:
					temp = [value['id'], value['name'], value['equipable_by_player'], None]
				else:
					temp = [value['id'], value['name'], value['equipable_by_player'], value['equipment']['requirements'], value['equipment']['slot']]
				# print(value['name'])
				# print(value['id'])
				if any(value['name'] in x for x in noDup):
					pass
				else:
					noDup.append(temp)

				#22429
			with open("/home/odu/Desktop/Programming/osrs-slayer-helper/backend/static/items.json", "w") as f:
				json.dump(noDup, f)

			print(len(noDup))
			print(noDup)

			return "success"

	def insert_items():
		noDup = []
		path = "/home/odu/Desktop/Programming/osrs-slayer-helper/backend/static/items.json"
		with open(path, "rb") as json_file:
			data = json.load(json_file)
			for i in data:
				new_item = ApiRepo().item_insert(i[0], i[1], i[2], i[4])
				# contains item id, name and boolean value of equipable

		return "success"

	def insert_item_req():
		all_item_req = []
		path = "/home/odu/Desktop/Programming/osrs-slayer-helper/backend/static/items.json"
		skillList = SkillRepo().get_all()
		with open(path, "rb") as json_file:
			data = json.load(json_file)
			for i in data:
				if i[2] == True and i[3] != None:
					for skill, level in i[3].items():
						for x in skillList:
							if x['name'].lower() == skill:
								skill_id = x['id']
								single_item = [i[0], skill_id, level]
								all_item_req.append(single_item)
				else:
					pass

			insert_item_reqs = ApiRepo().insert_item_req(all_item_req)

		return "hello"