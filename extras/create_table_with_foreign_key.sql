CREATE TABLE person_skill (
	person_id int NOT NULL,
    skill_id int NOT NULL,
    FOREIGN KEY (person_id) REFERENCES person(id),
    FOREIGN KEY (skill_id) REFERENCES skill(id)
    )