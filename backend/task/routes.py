from flask import Blueprint, jsonify, request
from backend.repo.task import TaskRepo
from backend.repo.task_item import TaskItemRepo

task = Blueprint('task', __name__)

@task.route('/slayer')
def get_all():
	tasks = TaskRepo().get_all()
	return jsonify(tasks)

@task.route('/slayer/id/<task_id>')
def id_task(task_id=None):
	task = TaskRepo().get_by_id(task_id)
	return jsonify(task)

@task.route('/slayer/name/<npc_name>')
def name_task(npc_name=None):
	task = TaskRepo().get_by_name(npc_name)
	return jsonify(task)

@task.route('/slayer/name/<npc_name>/setup', methods=['GET'])
def task_setup(npc_name=None):

	exists = TaskItemRepo().exists(npc_name)

	if exists != None:
		
		setups = TaskItemRepo().get_setups_by_name(npc_name)
		requirements = TaskItemRepo().get_setup_reqs(npc_name)
		for key in setups:
			setups[key].update(requirements.get(key, {}))
			# second parameter of get function is default value in case no requirements are found
		return jsonify(setups), 200

	else:
		return jsonify({"error": "There are currently no setups for this task"}), 204

@task.route('/slayer/name/<npc_name>/setup/requirements', methods=['GET'])
def task_setup_req(npc_name=None):

	exists = TaskItemRepo().exists(npc_name)

	if exists != None:
		
		setups = TaskItemRepo().get_setup_reqs(npc_name)
		
		return jsonify(setups), 200

@task.route('/slayer/name/<npc_name>/setup', methods=['POST'])
def insert_new_inventory(npc_name=None):

	data = request.get_json()
	npc_id = (TaskRepo().get_by_name(data['task'])['id'])
	

	exists = TaskItemRepo().exists(npc_name)
	
	if exists == None:

		setup_num = 1

		format_setup = TaskItemRepo().format_new_setup(data, npc_id, setup_num)

		equipped_list = format_setup['equipped']
		inventory_list = format_setup['inventory']

		insert_equipped = TaskItemRepo().insert_equipped(equipped_list)
		insert_inventory = TaskItemRepo().insert_inventory(inventory_list)

		return "Setup added"

	else:
		# if does exist take inputs and store as new setup
		setup_num = exists['setup']+1

		format_setup = TaskItemRepo().format_new_setup(data, npc_id, setup_num)

		equipped_list = format_setup['equipped']
		inventory_list = format_setup['inventory']

		insert_equipped = TaskItemRepo().insert_equipped(equipped_list)
		insert_inventory = TaskItemRepo().insert_inventory(inventory_list)

		return "Additional setup added"
	