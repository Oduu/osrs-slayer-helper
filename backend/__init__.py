from flask import Flask
from flask_cors import CORS, cross_origin

app = Flask(__name__, static_url_path='/backend/static')
CORS(app)

from backend.main.routes import main
from backend.hiscores.routes import hiscores
from backend.task.routes import task
from backend.item.routes import item

app.register_blueprint(main)
app.register_blueprint(hiscores)
app.register_blueprint(task)
app.register_blueprint(item)