from flask import Blueprint, jsonify
from backend.repo.osrs import ApiRepo
from backend.repo.person import PersonRepo
from backend.repo.skill import SkillRepo
from backend.repo.person_skill import PersonSkillRepo
import datetime

# using this website services.runescape.com/m=hiscore_oldschool/index_lite.ws?player=PlayerName

hiscores = Blueprint('hiscores', __name__)

@hiscores.route('/hiscores/<username>')
def hiscore(username):

	if len(username) > 12:
		# OSRS character names can only be 12 characters long
		return "Username too long", 404

	PR = PersonRepo()
	PSR = PersonSkillRepo()
	SR = SkillRepo()
	AR = ApiRepo()

	exists = PR.exists(username)
	if exists != None:

		last_updated = exists['updated']
		now = datetime.datetime.now()
		difference = (now-last_updated).total_seconds()

		# 7200 is 2 hours in seconds
		if difference > 7200:
			api_details = AR.get_details(username)
			update_details = PSR.update_skills(exists['id'], SR.get_all(), api_details)
			update_timestamp = PR.update_timestamp(exists['id'])
			get_updated_player = PR.get_person_stats(username)
			return jsonify(get_updated_player)

		else:
			db_details = PR.get_person_stats(username)
			player = PR.get_person_stats(username)
			return jsonify(player)

	else:
		api_details = AR.get_details(username)

		# as details are not stored in DB we need to make a call to the OSRS hiscores database to retrieve the details
		if api_details == None:
			# error handling in case hiscores does not have any data for the given username
			return jsonify({"error": "No content was found for that username"}), 404

		else:

			new_record = PR.create_player(username)
			create_links = PSR.create_link(new_record['id'], SR.get_all(), api_details)

			get_new_player = PR.get_person_stats(username)

			return jsonify(get_new_player)