from flask import Blueprint, jsonify
from backend.repo.item import ItemRepo
from backend.repo.conn import DB

item = Blueprint('item', __name__)

@item.route('/item')
def all_items():
	items = ItemRepo().get_all()
	return jsonify(items)

@item.route('/item/<item_id>')
def item_by_id(item_id=None):
	item = ItemRepo().get_by_id(item_id)
	return jsonify(item)

@item.route('/item/equippable')
def equippable_items():
	items = ItemRepo().equippable_items()
	return jsonify(items)
	
@item.route('/item/slot/<slot>')
def item_by_slot(slot=None):
	items = ItemRepo().get_by_slot(slot)
	return jsonify(items)