from backend.repo.mysql import MySQLRepo

class ItemReqRepo(MySQLRepo):
	def get_req_with_joins(self):
		self.cursor.execute(""" 
			SELECT item.name, skill.name, item_req.level FROM item_req
		 	INNER JOIN item ON item.id = item_req.item_id 
		 	INNER JOIN skill ON item_req.skill_id = skill.id 
		 """)
		return self.cursor.fetchall()

