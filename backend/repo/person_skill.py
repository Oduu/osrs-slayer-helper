from backend.repo.mysql import MySQLRepo
from backend.repo.skill import SkillRepo

class PersonSkillRepo(MySQLRepo):
	def create_link_old(self, person_id, skill_id, skill_level, skill_experience, skill_rank):
		self.cursor.execute("""
			INSERT INTO person_skill(person_id, skill_id, skill_level, skill_experience, skill_rank) VALUES(%s, %s, %s, %s, %s) """ \
			,(person_id, skill_id, skill_level, skill_experience, skill_rank))
		self.connection.commit()

	def create_link(self, person_id, skill_list, api_details):

		# there are 23 skills in OSRS and a total level stat which is why this range is here
		# there is additional information on the hiscores but this is all that is needed for now

		newData = []
		for x in range(0,24):
			skill_id = skill_list[x]['id']
			rank = api_details[x][0]
			level = api_details[x][1]
			xp = api_details[x][2]
			if int(level) < 0:
				level = None
			if int(rank) < 0:
				rank = None
			if int(xp) < 0:
				xp = None
			skillChunk = (person_id, skill_id, level, xp, rank)
			newData.append(skillChunk)

		self.cursor.executemany("""
			INSERT INTO person_skill(person_id, skill_id, skill_level, skill_experience, skill_rank) 
			VALUES(%s, %s, %s, %s, %s) """ \
			,(newData))
		self.connection.commit()

	def update_skills(self, person_id, skill_list, api_details):

		updatedSkills = []
		for x in range(0,24):
			skill_id = skill_list[x]['id']
			rank = api_details[x][0]
			level = api_details[x][1]
			xp = api_details[x][2]
			if int(level) < 0:
				level = None
			if int(rank) < 0:
				rank = None
			if int(xp) < 0:
				xp = None
			skillChunk = (level, xp, rank, person_id, skill_id)
			updatedSkills.append(skillChunk)

		self.cursor.executemany("""
			UPDATE person_skill 
			SET skill_level = %s, skill_experience = %s, skill_rank = %s 
			WHERE person_id = %s AND skill_id = %s """ \
			,(updatedSkills))
		self.connection.commit()


