from backend.repo.mysql import MySQLRepo

class TaskItemRepo(MySQLRepo):
	def get_equipped_by_name(self, npc_name):
		self.cursor.execute(""" 
			SELECT item.name, item.slot, item.id, task.npc_name, TI.equipped, TI.setup 
			FROM task_item AS TI
			INNER JOIN item ON TI.item_id = item.id
			INNER JOIN task ON task.id = TI.task_id
			WHERE npc_name = %s AND TI.equipped = 1 
			ORDER BY setup DESC """ \
			,(npc_name))

		return self.cursor.fetchall()

	def get_inventory_by_name(self, npc_name):
		self.cursor.execute(""" 
			SELECT item.name, item.slot, item.id, task.npc_name, TI.equipped, TI.setup
			FROM task_item AS TI
			INNER JOIN item ON TI.item_id = item.id
			INNER JOIN task ON task.id = TI.task_id
			WHERE npc_name = %s AND TI.equipped = 0 
			ORDER BY setup DESC """ \
			,(npc_name))

		return self.cursor.fetchall()

	def get_setups_by_name(self, npc_name):
		self.cursor.execute("""
			SELECT item.name, item.slot, item.id, task.npc_name, TI.equipped, TI.setup
			FROM task_item AS TI
			INNER JOIN item ON TI.item_id = item.id
			INNER JOIN task ON task.id = TI.task_id
			WHERE npc_name = %s
			ORDER BY setup DESC """ \
			,(npc_name))

		result = self.cursor.fetchall()

		if result == None:
			print("result is none")

		num_of_setups = result[0]['setup']
		setups = {}

		for i in range(1, num_of_setups+1):
			setups[i] = { "inventory": [], "equipped": [] }
			for item in result:
				if item['setup'] == i and item['equipped'] == 1:
					setups[i]['equipped'].append(item)
				if item['setup'] == i and item['equipped'] == 0:
					setups[i]['inventory'].append(item)

		return setups

	def get_setup_reqs(self, npc_name):
		self.cursor.execute("""
			SELECT TI.setup, IR.level, skill.name AS skill
			FROM task_item AS TI
			INNER JOIN item ON TI.item_id = item.id
			INNER JOIN task ON task.id = TI.task_id
            LEFT JOIN item_req AS IR ON item.id = IR.item_id
            LEFT JOIN skill ON skill.id = IR.skill_id
			WHERE npc_name = %s
			ORDER BY setup DESC """ \
			,(npc_name))

		results = self.cursor.fetchall()

		num_of_setups = results[0]['setup']
		setups = {}

		for i in range(1, num_of_setups+1):
			setups[i] = { "requirements": [] }
			for item_requirement in results:
				if item_requirement['setup'] == i:
					if item_requirement['skill'] != None:
						if any(d['skill'] == item_requirement['skill'] for d in setups[i]['requirements']):
							for requirement in setups[i]['requirements']:
								if requirement['skill'] == item_requirement['skill']:
									if requirement['level'] < item_requirement['level']:
										requirement['level'] = item_requirement['level']
						else:
							setups[i]['requirements'].append(item_requirement)

		return setups


	def insert_equipped(self, equipped_list):
		self.cursor.executemany("""
			INSERT INTO task_item(item_id, task_id, equipped, setup) VALUES(%s, %s, 1, %s) """ \
			,(equipped_list))
		self.con.commit()

	def insert_inventory(self, inventory_list):
		self.cursor.executemany("""
			INSERT INTO task_item(item_id, task_id, equipped, setup) VALUES(%s, %s, 0, %s) """ \
			,(inventory_list))
		self.con.commit()

	def exists(self, npc_name):
		self.cursor.execute("""
			SELECT item.name, item.slot, item.id, task.npc_name, TI.equipped, TI.setup 
			FROM task_item AS TI
			INNER JOIN item ON TI.item_id = item.id
			INNER JOIN task ON task.id = TI.task_id
			WHERE npc_name = %s 
			ORDER BY setup DESC
			""" \
			,(npc_name))
		return self.cursor.fetchone()

	def format_new_setup(self, data, npc_id, setup_num):

		equipped = data['equipped']
		inventory = data['inventory']

		equipped_list = []
		inventory_list = []

		for key, value in inventory.items():
			if value['item'] != None:
				invItem = (value['item']['id'], npc_id, setup_num)
				for i in range(0, value['quantity']):
					inventory_list.append(invItem)

		for key, value in equipped.items():
			if value != {}:
				equipped_list.append((value['id'], npc_id, setup_num))

		return {"equipped": equipped_list, "inventory": inventory_list}