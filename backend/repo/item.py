from backend.repo.mysql import MySQLRepo

class ItemRepo(MySQLRepo):
	def get_all(self):
		self.cursor.execute(""" SELECT id, name, equippable, slot FROM item """)
		return self.cursor.fetchall()

	def get_by_id(self, item_id):
		self.cursor.execute(""" SELECT id, name, equippable, slot FROM item WHERE id = %s """ \
			,(item_id))
		return self.cursor.fetchone()

	def equippable_items(self):
		self.cursor.execute(""" SELECT id, name, equippable, slot FROM item WHERE equippable = 1 """ )
		return self.cursor.fetchall()

	def get_by_slot(self, slot):
		self.cursor.execute(""" SELECT id, name, equippable, slot from item WHERE slot = %s """ \
			,(slot))
		return self.cursor.fetchall()