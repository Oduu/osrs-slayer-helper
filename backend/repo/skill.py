from backend.repo.mysql import MySQLRepo

class SkillRepo(MySQLRepo):
	def get_all(self):
		self.cursor.execute(""" SELECT id,name FROM skill """)
		return self.cursor.fetchall()