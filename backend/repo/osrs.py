from backend.repo.mysql import MySQLRepo
import requests

class ApiRepo(MySQLRepo):
	def get_details(self, username):
		url = f'https://secure.runescape.com/m=hiscore_oldschool/index_lite.ws?player={username}'
		response = requests.get(url)
		if (response.status_code) == 200:
			response_split = response.text.splitlines()
			formatted = []
			for item in response_split:
				formatted.append(item.split(","))

			return formatted
		else:
			return None

	def item_insert(self, item_id, item_name, equipable, slot):
		self.cursor.execute(""" INSERT INTO item(id, name, equipable, slot) VALUES (%s, %s, %s, %s) """ \
			,(item_id, item_name, equipable, slot))
		self.connection.commit()

	def insert_item_req(self, item_req_info):
		self.cursor.executemany(""" INSERT INTO item_req(item_id, skill_id, level) VALUES (%s, %s, %s) """ \
			,(item_req_info))
		self.connection.commit()