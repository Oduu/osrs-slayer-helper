from backend.repo.mysql import MySQLRepo

class TaskRepo(MySQLRepo):
	def get_all(self):
		self.cursor.execute(""" SELECT id, npc_name, level_req FROM task """)
		return self.cursor.fetchall()

	def get_by_id(self, task_id):
		self.cursor.execute(""" SELECT id, npc_name, level_req FROM task WHERE id = %s """ \
			,(task_id))
		return self.cursor.fetchone()

	def get_by_name(self, npc_name):
		self.cursor.execute(""" SELECT id, npc_name, level_req FROM task WHERE npc_name = %s """ \
			,(npc_name))
		return self.cursor.fetchone()