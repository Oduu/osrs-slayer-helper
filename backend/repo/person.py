from backend.repo.mysql import MySQLRepo

class PersonRepo(MySQLRepo):
	def exists(self, username):
		#returns None if doesn't exist
		self.cursor.execute(""" SELECT id, username, updated FROM person WHERE username = %s """ \
			,(username))
		return self.cursor.fetchone()

	def create_player(self, username):
		self.cursor.execute("""
			INSERT INTO person(username) VALUES(%s) 
			""" \
			,(username))
		self.connection.commit()

		created_user = self.exists(username)
		return created_user
		
	def get_person_stats(self, username):
		self.cursor.execute(""" 
			SELECT name, skill_level, skill_rank, skill_experience FROM person_skill as PS
			INNER JOIN skill as S ON S.id = PS.skill_id
			INNER JOIN person AS P ON P.id = PS.person_id 
			WHERE P.username = %s
			ORDER BY S.id
			""" \
			,(username))
		return self.cursor.fetchall()

	def update_timestamp(self, person_id):
		self.cursor.execute("""
			UPDATE person SET updated = now() WHERE id = %s
			""" \
			,(person_id))
		self.connection.commit()