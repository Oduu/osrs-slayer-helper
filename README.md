# Slayer Assistant

## Features

The core features of this app are highlighted on the left hand side navbar. 

These being:
    
    - Home Page
        - This is where the user will input their username and desired slayer task
        - The rest of the app is populated after these fields are inputted and submitted

![](https://i.imgur.com/HPWk96u.png)
        
    - Hiscores
        - This retrieves the specified character's in game levels from the Jagex API
        - The details are then stored in a MySQL database with a timestamp
        - For every subsequent request of this user's hiscores, if the timestamp is older than 2 hours then the details are requested again and updated
            - This is helpful as the Jagex API tends to be quite slow (especially with the issues it has been having since Dec 2019)
    
![](https://i.imgur.com/qYz0Tq6.png)
    
    - Setup
        - This page contains the equipped and inventory items that the user should take to complete their task efficiently
        - If you don't know what an inventory item is, just hover over it to see the item name in the tooltip
        - In-game location to kill the monster to be added here along with any "extra information" such as rune pouch contents
        
![](https://i.imgur.com/Bw2wJWb.png)
        
    - Create
        - If a user finds that a setup is missing from their desired task they can add the information for other players to use here
        - The page is split into two sections, one for equipment and one for inventory (mirroring the setup page layout)
        - Using the v-select library for the dropdowns, users can select any item and quantity to take on their journey and can add extra inventory slots if required.
            - aslong as it does not exceed maximum inventory size
        
![](https://i.imgur.com/UPXdjo7.png)
        
        
    
    

