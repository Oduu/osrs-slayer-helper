import Vue from 'vue';
import vSelect from 'vue-select'
import App from './App.vue';
import router from './router';
import store from './store';
import 'bootstrap'; import 'bootstrap/dist/css/bootstrap.min.css';
import "vue-select/dist/vue-select.css";
import "@/assets/global.css"
import Navbar from './components/Navbar.vue'
import EquippedSelector from './components/EquippedSelector.vue'
import InventorySelector from './components/InventorySelector.vue'
import TaskSelector from './components/TaskSelector.vue'


Vue.config.productionTip = false

Vue.component('Navbar', Navbar);
Vue.component('EquippedSelector', EquippedSelector);
Vue.component('InventorySelector', InventorySelector);
Vue.component('TaskSelector', TaskSelector);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
