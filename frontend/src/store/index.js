import Vue from 'vue';
import Vuex from 'vuex';
import items from './modules/items';
import tasks from './modules/tasks';
import setups from './modules/setups';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  	tasks,
  	items,
    setups
  }
})
