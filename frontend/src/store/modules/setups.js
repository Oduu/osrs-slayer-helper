import axios from 'axios';

const state = {

	setups: null,

	currentSetup: null,

	errorMsg: "",

	validSetups: []

};

const getters = {

	allSetups: (state) => (state.setups),
	currentSetup: (state) => (state.currentSetup),
	getSetupError: (state) => (state.errorMsg),
	getValidSetups: (state) => (state.validSetups)

};

const actions = {

	async fetchSetups( {commit, state}, npc_name ) {

		let res = await axios.get(`http://localhost:5000/slayer/name/${npc_name}/setup`);

		if(res.status == 200) {
			let response = {status: "success", message: "Setups loaded"};
			commit('setSetup', res.data);
			commit('setError', response);
			
		} else {
			let response = {status: "error", message: "No setups were found for the selected task"};
			commit('setError', response);
		}

		

	},

	updateCurrentSetup( {commit, state}, setup_number ) {

		let current_setup = state.setups[setup_number]

		commit('setCurrentSetup', current_setup)

	},

	validSetups( {commit, state}, skills ) {

		let setupIndex = 0;

		for(let key in state.setups) {
			// loop over requirements array in current setup
			setupReqLoop: for(let requirement in state.setups[key]['requirements']) {
				let item_requirement = (state.setups[key]['requirements'][requirement])
				//  loop over and check if setup requirement is met by current character skill level
				for(let i = 0; i < skills.length; i++) {
					if(item_requirement['skill'] == skills[i]['name']) {

						// console.log(`the item requirement skill is ${item_requirement['skill']} and ${item_requirement['level']}`)
						// console.log(`the character skill is ${skills[i]['name']} and ${skills[i]['skill_level']}`)

						if(item_requirement['level'] <= skills[i]['skill_level']) {
							setupIndex = parseInt(key);

							// requirement met

						} else {
							setupIndex = 0;
							break setupReqLoop;
							// requirement not met so move onto next iteration
							
						}
						
					}
				}
			}
			if(setupIndex == key) {
				// all requirements met for current setup
				commit('addValidSetup', setupIndex);
			}
		}

	},

};

const mutations = {

	setSetup: (state, setups) => (state.setups = setups),
	setCurrentSetup: (state, current_setup) => (state.currentSetup = current_setup),
	setError: (state, response) => (state.errorMsg = response),
	addValidSetup: (state, setupIndex) => (state.validSetups.push( setupIndex))

};

export default {
	state,
	getters,
	actions,
	mutations
};