import axios from 'axios';

const state = {

	tasks: [],

	selected: "",

	current: ""
	

};

const getters = {

	allTasks: (state) => state.tasks,
	getSelected: (state) => state.selected


};

const actions = {

	async fetchTasks( { commit } ) {

		const res = await axios.get('http://localhost:5000/slayer');

		commit('setTasks', res.data);

	},

	updateSelected( {commit}, selected ) {

		commit('setUpdated', selected)

	}

};

const mutations = {

	setTasks: (state, tasks) => (state.tasks = tasks),
	setUpdated: (state, selected) => (state.selected = selected)

};

export default {
	state, 
	getters, 
	actions,
	mutations
};