import axios from 'axios';

const state = {

	items: [

	],

	new_equipped: {

		head: {},
		neck: {},
		body: {},
		legs: {},
		feet: {},
		hands: {},
		cape: {},
		weapon: {},
		shield: {},
		ring: {},
		ammo: {}

	},

	new_inventory: {

		// 0: { item: null, quantity: 0 },
		1: { item: null, quantity: 0 },// 28 slots here then use index prop to pass through
		2: { item: null, quantity: 0 },
		3: { item: null, quantity: 0 },
		4: { item: null, quantity: 0 },
		5: { item: null, quantity: 0 },
		6: { item: null, quantity: 0 },
		7: { item: null, quantity: 0 },
		8: { item: null, quantity: 0 },
		9: { item: null, quantity: 0 },
		10: { item: null, quantity: 0 },
		11: { item: null, quantity: 0 },
		12: { item: null, quantity: 0 },
		13: { item: null, quantity: 0 },
		14: { item: null, quantity: 0 },
		15: { item: null, quantity: 0 },
		16: { item: null, quantity: 0 },
		17: { item: null, quantity: 0 },
		18: { item: null, quantity: 0 },
		19: { item: null, quantity: 0 },
		20: { item: null, quantity: 0 },
		21: { item: null, quantity: 0 },
		22: { item: null, quantity: 0 },
		23: { item: null, quantity: 0 },
		24: { item: null, quantity: 0 },
		25: { item: null, quantity: 0 },
		26: { item: null, quantity: 0 },
		27: { item: null, quantity: 0 },
		28: { item: null, quantity: 0 },

	}

};

const getters = {

	allItems: (state) => (state.items),

};

const actions = {

	async fetchItems( {commit, state} ) {

		if(state.items.length == 0) {

			// console.log("sending request")

			let res = await axios.get(`http://localhost:5000/item`);


			commit('setItems', res.data);

			// dispatch('anotherFunction', parameter);

		}

	},

	updateEquipped( { commit }, selectedItem ) {
		let selectedSlot = selectedItem['slot']
		// console.log(selectedSlot)
		commit('setEquipped', { selectedItem, selectedSlot })
	},

	updateInventory( { commit }, selectedItem ) {
		let itemIndex = selectedItem['index']
		let item = { item: selectedItem['item'], quantity: selectedItem['quantity'] }

		// better to have the hardcoded slots removed and have logic here to check if exists
		// if not push it
		// if it does update it
		commit('setInventory', { item, itemIndex })

	}

};

const mutations = {

	setItems: (state, items) => (state.items = items),
	setEquipped: (state, selected) => (state.new_equipped[selected.selectedSlot] = selected.selectedItem),
	setInventory: (state, selected) => (state.new_inventory[selected.itemIndex] = selected.item)

};

export default {
	state, 
	getters, 
	actions,
	mutations
};