import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Setup from '../views/Setup.vue'
import New_setup from '../views/New_setup.vue'
import Hiscores from '../views/Hiscores.vue'

Vue.use(VueRouter)

const routes = [
  { path: '/', name: 'home', component: Home },
  { path: '/setup', name: 'setup', component: Setup },
  { path: '/hiscores', name: 'hiscores', component: Hiscores },
  { path: '/new', name: 'New_setup', component: New_setup },
  { path: '*', name: 'NotFound', component: Home }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
